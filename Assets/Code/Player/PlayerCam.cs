using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{
    public static PlayerCam main;
    
    Transform focusTransform;

    [SerializeField] float camBounds = 2f;
    [SerializeField] float aimOffset = 0.5f;
    [SerializeField] float camDelay = 0.5f;
    [SerializeField] float yOffset = 5f;
    [SerializeField] float facingOffset = 3f;
    float initialZoom;

    Vector3 targetPosition;
    Vector3 initialTargetPosition;
    Vector2 camBoundsAdjusted;
    Vector3 forwardOffset;

    Entity player;

    void Start()
    {
        main = this;

        camBoundsAdjusted = new Vector2(camBounds, camBounds / Camera.main.aspect);

        SetNewFocusTransform(GameManager.main.player.aimTransform);

        targetPosition = new Vector3(focusTransform.position.x, focusTransform.position.y, transform.position.z);
        initialTargetPosition = targetPosition;

        initialZoom = targetPosition.z;

        player = GameManager.main.player;

        forwardOffset = Vector3.zero;
    }

    void LateUpdate()
    {
        SetTargetPosition();

        transform.DOMove(targetPosition, camDelay);
    }

    public void SetNewFocusTransform(Transform newFocusTransform)
    {
        focusTransform = newFocusTransform;
        player = GameManager.main.player;
    }

    private void SetTargetPosition()
    {
        Vector3 distCamToFocus = focusTransform.position - initialTargetPosition;

        //If the target position is outside of the camera bounds, then update the target position so that it's inside the camera bounds
        //Cam Bounds are halved as because the distance between positive x/y and negative x/y is double the value entered
        if (distCamToFocus.x > camBoundsAdjusted.x / 2)
        {
            initialTargetPosition += new Vector3(distCamToFocus.x - camBoundsAdjusted.x / 2, 0, 0);
        }

        else if (distCamToFocus.x < -camBoundsAdjusted.x / 2)
        {
            initialTargetPosition += new Vector3(distCamToFocus.x + camBoundsAdjusted.x / 2, 0, 0);
        }

        if (distCamToFocus.y > camBoundsAdjusted.y / 2)
        {
            initialTargetPosition += new Vector3(0, distCamToFocus.y - camBoundsAdjusted.y / 2, 0);
        }

        else if (distCamToFocus.y < -camBoundsAdjusted.y / 2)
        {
            initialTargetPosition += new Vector3(0, distCamToFocus.y + camBoundsAdjusted.y / 2, 0);
        }

        Vector3 aimVector = Vector3.zero;

        //Add offset based on aim direction. Should handle differently is camera can be fully taken over for gameplay highlighting
        //if (PlayerInputHandler.main.rangedInput >= 0.2)
        //{
        //    aimVector = Quaternion.Euler(GameManager.main.player.rotationTarget) * -Vector3.right;
        //}

        if (Mathf.Abs(player.rb.velocity.normalized.x) > 0)
        {
            forwardOffset = new Vector3(player.rb.velocity.normalized.x * facingOffset * player.rb.useGravity.GetHashCode(), 0, 0);
        }

        targetPosition = initialTargetPosition + new Vector3(0,yOffset,0) * player.rb.useGravity.GetHashCode() + forwardOffset + Vector3.Scale(aimVector, new Vector3(aimOffset, aimOffset / Camera.main.aspect, aimOffset));
    }
}
