using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shield", menuName = "TwinStickVania/Spell/Shield")]
public class ShieldSpell : Spell
{
    GameObject shieldPrefab;
    public override void UseSpell()
    {
        UseShield();
    }

    public override void EndSpell()
    {
        DisableShield();
    }

    private void UseShield()
    {
        spellActive = true;

        shieldPrefab = Instantiate(prefab, GameManager.main.player.transform);

        //GameManager.main.player.invincible = true;

        shieldPrefab.SetActive(true);
    }

    private void DisableShield()
    {
        spellActive = false;

        //GameManager.main.player.invincible = false;

        Destroy(shieldPrefab);
    }

}
