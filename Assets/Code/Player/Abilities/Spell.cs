using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spell : ScriptableObject
{
    [Header("Spell")]
    public new string name = "Name";
    public Sprite icon;

    [Header("Attributes")]
    public int manaCost = 100;
    public float cooldown = 5f;
    public float activeLength;
    [Space (20)]
    [Header("Visuals")]
    public GameObject prefab;

    [HideInInspector] public bool spellActive = false;

    public abstract void UseSpell();

    public abstract void EndSpell();

}
