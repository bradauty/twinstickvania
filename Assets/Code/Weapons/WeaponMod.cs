using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeaponMod", menuName = "TwinStickVania/Weapon Mod")]
public class WeaponMod : ScriptableObject
{
    [Header("Weapon Mod")]
    public new string name = "Name";
    public Sprite icon;

    [Header("Modifiers")]
    public bool charge = false;
    public bool homing = false;
    public bool wobble = false;
    public bool cluster = false;
    [Space(20)]
    public float projSizeMulti = 1;
    public float lifetimeMulti = 1;
    public float speedMulti = 1;
    public float damageMulti = 1;
    public float force = 0;
    public float wobbleSpeed = 0;
    public float wobbleMagnitude = 0;
    [Space(20)]
    public float rateOfFireMulti = 1;
    public float overheatMulti = 1;
    public int projMulti = 1;
    public float projForwardOffset = 0;
    public Weapon.DamageType dmgType = Weapon.DamageType.Normal;
    public float statusBuildUp = 0;
}
