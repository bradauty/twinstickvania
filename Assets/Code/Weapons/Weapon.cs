using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

public class Weapon : MonoBehaviour
{
    public enum DamageType { Normal, Fire, Ice, Steam, Slow, Light, Dark, Pure }

    //Default Values
    [TitleGroup("Weapon Defaults")]
    public bool damagePlayer = false;
    public bool damageEnemies = true;
    [SerializeField] float baseHPDamage = 5;
    [SerializeField] float baseStaminaDamage = 10;
    [SerializeField] float baseSpeed = 10;
    [SerializeField] float baseRateOfFire = 10;
    [SerializeField] float baseLifetime = 3;
    [Space(20)]
    [SerializeField] bool chargeDefault = false;
    [SerializeField] bool homingDefault = false;
    [SerializeField] bool wobbleDefault = false;
    [SerializeField] bool clusterDefault = false;
    [Space(20)]
    [SerializeField] float baseOverheat = 0;
    [SerializeField] float baseForce = 0;
    [SerializeField] float baseSize = 1;
    [SerializeField] int baseProjPerShot = 1;
    [SerializeField] float baseOffset = 0;
    [SerializeField] float baseWobbleSpeed = 0;
    [SerializeField] float baseWobbleMagnitude = 0;
    [Space(20)]
    [SerializeField] DamageType baseDmgType = DamageType.Normal;
    [SerializeField] float baseStatusBuildUp = 0;

    [TitleGroup("Mods")]
    [SerializeField] WeaponMod mod1;
    [SerializeField] WeaponMod mod2;
    [ButtonGroup]
    [Button(ButtonSizes.Medium)]
    private void UpdateMods()
    {
        AssignMods(mod1, mod2);
    }

    [TitleGroup("Technical Stuff")]
    [SerializeField] Transform projectileSpawnPoint;
    [SerializeField] GameObject bulletPrefab;
    [Space(20)]
    [SerializeField] WeaponMod defaultMod;

    private Bullet bullet;

    //Weapon Parameters
    private bool charge;

    private int projPerShot = 1;
    private float projOffset = 0;
    
    public float rateOfFire = 1;
    private float overheat = 1;

    //These parameters can probably be applied directly to the bullet object rather than stored here
    //Projectile Parameters
    private bool homing;
    private bool cluster;
    private bool wobble;

    private float damage;
    private float speed;
    private float lifetime;
    private float size;
    private float force = 0;
    private float wobbleSpeed;
    private float wobbleMagnitude;

    private DamageType dmgType = DamageType.Normal;
    private float statusBuildUp = 0;

    //Utility
    float timeSinceFire;


    void Start()
    {
        timeSinceFire = 1/baseRateOfFire;

        bullet = bulletPrefab.GetComponent<Bullet>();

        //temp
        AssignMods(mod1, mod2);
    }

    void Update()
    {
        timeSinceFire += Time.deltaTime;
    }

    public void AssignMods(WeaponMod newMod1)
    {
        mod1 = newMod1;

        mod2 = defaultMod;

        SetUpWeapon();
        UpdateProjectileStats();
    }

    public void AssignMods(WeaponMod newMod1, WeaponMod newMod2)
    {
        mod1 = newMod1;
        mod2 = newMod2;

        SetUpWeapon();
        UpdateProjectileStats();
    }

    public void SetUpWeapon()
    {
        //Weapon Parameters

        if (mod1.charge || mod2.charge)
        {
            charge = true;
        }
        else { charge = chargeDefault; }

        projPerShot = baseProjPerShot * mod1.projMulti * mod2.projMulti;
        projOffset = baseOffset + mod1.projForwardOffset + mod2.projForwardOffset;

        rateOfFire = baseRateOfFire * mod1.rateOfFireMulti * mod2.rateOfFireMulti;
        overheat = baseOverheat * mod1.overheatMulti * mod2.overheatMulti;


        //Projectile Parameters
        
        if (mod1.homing || mod2.homing)
        {
            homing = true;
        }
        else { homing = homingDefault; }

        if (mod1.wobble || mod2.wobble)
        {
            wobble = true;
        }
        else { wobble = wobbleDefault; }

        if (mod1.cluster || mod2.cluster)
        {
            cluster = true;
        }
        else { cluster = clusterDefault; }

        damage = baseHPDamage * mod1.damageMulti * mod2.damageMulti;
        speed = baseSpeed * mod1.speedMulti * mod2.speedMulti;
        lifetime = baseLifetime * mod1.lifetimeMulti * mod2.lifetimeMulti;
        size = baseSize * mod1.projSizeMulti * mod2.projSizeMulti;
        force = baseForce + mod1.force + mod2.force;
        wobbleSpeed = baseWobbleSpeed + mod1.wobbleSpeed + mod2.wobbleSpeed;
        wobbleMagnitude = baseWobbleMagnitude + mod1.wobbleMagnitude + mod2.wobbleMagnitude;

        //Temporary solution to damage types (only allows for 1 damage type, prioritising damage type from mod2)
        if (mod1.dmgType != DamageType.Normal)
        {
            dmgType = mod1.dmgType;
        }

        else if (mod2.dmgType != DamageType.Normal)
        {
            dmgType = mod2.dmgType;
        }

        else
        {
            dmgType = baseDmgType;
        }

        statusBuildUp = baseStatusBuildUp + mod1.statusBuildUp + mod2.statusBuildUp;
    }

    public void UpdateProjectileStats()
    {
        bullet.homing = homing;
        bullet.cluster = cluster;
        bullet.wobble = wobble;

        bullet.hpDamage = damage;
        bullet.speed = speed;
        bullet.lifetime = lifetime;
        bullet.size = size;
        bullet.force = force;
        bullet.wobbleSpeed = wobbleSpeed;
        bullet.wobbleMagnitude = wobbleMagnitude;

        bullet.dmgType = dmgType;
        bullet.statusBuildUp = statusBuildUp;

        bullet.damagePlayer = damagePlayer;
        bullet.damageEnemies = damageEnemies;
    }

    public void UpdateProjectileStats(Bullet spawnedBullet)
    {
        spawnedBullet.homing = homing;
        spawnedBullet.cluster = cluster;
        spawnedBullet.wobble = wobble;

        spawnedBullet.hpDamage = damage;
        spawnedBullet.speed = speed;
        spawnedBullet.lifetime = lifetime;
        spawnedBullet.size = size;
        spawnedBullet.force = force;
        spawnedBullet.wobbleSpeed = wobbleSpeed;
        spawnedBullet.wobbleMagnitude = wobbleMagnitude;

        spawnedBullet.dmgType = dmgType;
        spawnedBullet.statusBuildUp = statusBuildUp;

        spawnedBullet.damagePlayer = damagePlayer;
        spawnedBullet.damageEnemies = damageEnemies;
    }

    public void Charge()
    {
        //If weapon is a charge weapon build up meter and fire on release
    }

    public void Fire()
    {
        if(timeSinceFire >= 1/rateOfFire)
        {
            float initialOffset = (projPerShot - 1) * -projOffset;
            
            for (int i = 0; i < projPerShot; i++)
            {
                initialOffset += projOffset;

                Quaternion spawnDir = Quaternion.Euler(new Vector3(projectileSpawnPoint.rotation.eulerAngles.x, projectileSpawnPoint.rotation.eulerAngles.y, projectileSpawnPoint.rotation.eulerAngles.z + initialOffset));

                //offset required due to projectile prefabs initial direction
                spawnDir *= Quaternion.Euler(new Vector3(-90, 0, 0));

                //Creating variable to reference in physics method below
                GameObject freshBullet = Instantiate(bullet.gameObject, projectileSpawnPoint.position, spawnDir);
                UpdateProjectileStats(freshBullet.GetComponent<Bullet>());
                //Ignoring the collisions between bullets and spawning entities
                Physics.IgnoreCollision(freshBullet.GetComponent<Collider>(), GetComponentInParent<Collider>());

                timeSinceFire = 0;

                if (bullet.flash != null)
                {
                    var flashInstance = Instantiate(bullet.flash, projectileSpawnPoint.position, spawnDir, transform);
                    var flashPs = flashInstance.GetComponent<ParticleSystem>();
                    if (flashPs != null)
                    {
                        Destroy(flashInstance, flashPs.main.duration);
                    }
                    else
                    {
                        var flashPsParts = flashInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                        Destroy(flashInstance, flashPsParts.main.duration);
                    }
                }
            }
        }
    }
}
