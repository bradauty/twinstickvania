using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float hitOffset = 0f;
    public bool UseFirePointRotation;
    public Vector3 rotationOffset = new Vector3(0, 0, 0);
    public GameObject hit;
    public GameObject flash;
    public GameObject[] Detached;

    public bool homing = false;
    public bool cluster = false;
    public bool wobble = false;

    public float hpDamage = 1;
    public float staminaDamage = 1;
    public float speed = 1;
    public float lifetime = 1;
    public float size = 1;
    public float force = 0;
    public float wobbleSpeed = 0;
    public float wobbleMagnitude = 0;

    public Weapon.DamageType dmgType = Weapon.DamageType.Normal;
    public float statusBuildUp = 0;

    public bool damagePlayer = false;
    public bool damageEnemies = false;

    Rigidbody rb;
    float currentLifetime = 0;
    float timeOffset;
    Collider col;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();

        gameObject.transform.localScale *= size;

        transform.rotation *= Quaternion.Euler(rotationOffset);

        timeOffset = Time.time;

        ParticleSystem[] pSystems = GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem pSys in pSystems)
        {
            if (!pSys.main.loop)
            {
                var main = pSys.main;
                main.startLifetime = lifetime;
            }
        }
    }

    private void Update()
    {
        currentLifetime += Time.deltaTime;

        if (currentLifetime > lifetime)
        {
            //in future implement a "fizzle out" rather than just destroying
            Destroy(gameObject);
        }
    }

    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        if (wobble)
        {
            rb.MovePosition(rb.position + (transform.forward * Time.deltaTime * speed) + (transform.right * Mathf.Sin(((Time.time - timeOffset) * wobbleSpeed) - speed) * wobbleMagnitude));
        }

        else if (homing)
        {
            //TO DO
            rb.MovePosition(rb.position + transform.forward * Time.deltaTime * speed);
        }

        else
        {
            rb.MovePosition(rb.position + transform.forward * Time.deltaTime * speed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        string colliderTag = collision.gameObject.tag;
        Rigidbody colliderRb = collision.rigidbody;
        
        //Apply force
        if (colliderRb != null && colliderTag != "Bullet")
        {
            Vector3 finalForce = (collision.transform.position - transform.position).normalized * force;

            colliderRb.AddForceAtPosition(finalForce, transform.position);

            colliderRb.AddExplosionForce(force, transform.position, 1);
        }

        if (colliderTag == "Enemy" && damageEnemies)
        {
            collision.gameObject.GetComponent<Entity>().ModifyResourceValue("hp", -hpDamage);
            collision.gameObject.GetComponent<Entity>().ModifyResourceValue("stamina", -staminaDamage);

            //Adds 5 mana when a bullet hits
            //Should probably update in the future to apply this change to the source entity. Will require a refactor of weapons and/or bullets to be aware of source
            GameManager.main.player.ModifyResourceValue("mana", 5);

            DestroyBullet(collision);
        }

        else if (colliderTag == "Player" && damagePlayer)
        {
            GameManager.main.player.ModifyResourceValue("hp", -hpDamage);
            DestroyBullet(collision);
        }

        else if (colliderTag == "Bullet")
        {
            Physics.IgnoreCollision(col, collision.collider);
        }

        else
        {
            //Makes sure to destroy bullet on other collisions like walls
            DestroyBullet(collision);
        }
    }

    void DestroyBullet(Collision collision)
    {
        //Lock all axes movement and rotation
        rb.constraints = RigidbodyConstraints.FreezeAll;
        speed = 0;

        ContactPoint contact = collision.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point + contact.normal * hitOffset;

        if (hit != null)
        {
            var hitInstance = Instantiate(hit, pos, rot);
            if (UseFirePointRotation) { hitInstance.transform.rotation = gameObject.transform.rotation * Quaternion.Euler(0, 180f, 0); }
            else if (rotationOffset != Vector3.zero) { hitInstance.transform.rotation = Quaternion.Euler(rotationOffset); }
            else { hitInstance.transform.LookAt(contact.point + contact.normal); }

            var hitPs = hitInstance.GetComponent<ParticleSystem>();
            if (hitPs != null)
            {
                Destroy(hitInstance, hitPs.main.duration);
            }
            else
            {
                var hitPsParts = hitInstance.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitInstance, hitPsParts.main.duration);
            }
        }
        foreach (var detachedPrefab in Detached)
        {
            if (detachedPrefab != null)
            {
                detachedPrefab.transform.parent = null;
            }
        }
        Destroy(gameObject);
    }

    public void ToggleDamageReceivers()
    {
        damageEnemies = !damageEnemies;
        damagePlayer = !damagePlayer;
    }

}
