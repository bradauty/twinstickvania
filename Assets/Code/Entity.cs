using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;

public class Entity : MonoBehaviour
{
    //Set in the Inspector
    public EntityStats entityStats;
    public Transform aimTransform;
    public Weapon weapon;
    //Separate into placeholder visuals script
    public LineRenderer lineRenderer;

    //Stat instances for this Entity
    public Dictionary<string, float> resources { get; private set; }
    public Dictionary<string, float> stats { get; private set; }
    public Dictionary<string, float> cooldowns { get; private set; }
    public Dictionary<string, bool> boolChecks { get; private set; }

    public Transform currentFocus { get; private set; }

    public Rigidbody rb { get; private set; }

    public MovementController mc;

    [ButtonGroup]
    [Button(ButtonSizes.Medium)]
    private void RefreshStats()
    {
        InitialiseStats();
    }

    void Awake()
    {
        weapon = GetComponentInChildren<Weapon>();
        rb = GetComponent<Rigidbody>();

        InitialiseStats();

        mc = GetComponent<MovementController>();
    }

    private void Update()
    {
        UpdateCooldowns();
    }

    public void InitialiseStats()
    {
        stats = new Dictionary<string, float>(entityStats.stats);
        cooldowns = new Dictionary<string, float>(entityStats.cooldowns);
        resources = new Dictionary<string, float>(entityStats.resources);
        boolChecks = new Dictionary<string, bool>();
        for (int i = 0; i < entityStats.boolChecks.Count; i++)
        {
            boolChecks.Add(entityStats.boolChecks[i], false);
        }
    }

    public void SetFocusTransform(Transform newFocus)
    {
        currentFocus = newFocus;
    }

    public void ClearFocusTransform()
    {
        currentFocus = null;
    }

    public bool GetBool(string boolName)
    {
        if(boolChecks.ContainsKey(boolName))
        {
            return boolChecks[boolName];
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Bool: " + boolName);
            return false;
        }
    }

    public void SetBool(string boolName, bool boolState)
    {
        if (boolChecks.ContainsKey(boolName))
        {
            boolChecks[boolName] = boolState;
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Bool: " + boolName);
        }
    }

    public bool CheckCooldown(string cooldownName)
    {
        if (cooldowns.ContainsKey(cooldownName))
        {
            return cooldowns[cooldownName] >= entityStats.cooldowns[cooldownName];
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Cooldown: " + cooldownName);
            return false;
        }
    }

    public void ResetCooldown(string cooldownName)
    {
        if (cooldowns.ContainsKey(cooldownName))
        {
            cooldowns[cooldownName] = 0;
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Cooldown: " + cooldownName);
        }
    }

    private void UpdateCooldowns()
    {
        foreach (KeyValuePair<string, float> cooldown in cooldowns.ToArray<KeyValuePair<string, float>>())
        {
            cooldowns[cooldown.Key] += Time.deltaTime;
        }
    }

    public float GetResourceValue(string resourceName)
    {
        if (resources.ContainsKey(resourceName))
        {
            return resources[resourceName];
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Resource: " + resourceName);
            return 0;
        }
    }

    public float GetMaxResourceValue(string resourceName)
    {
        if (entityStats.resources.ContainsKey(resourceName))
        {
            return entityStats.resources[resourceName];
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Resource: " + resourceName);
            return 0;
        }
    }

    public void ResetResourceValue(string resourceName)
    {
        if (entityStats.resources.ContainsKey(resourceName))
        {
            resources[resourceName] = entityStats.resources[resourceName];
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Resource: " + resourceName);
        }
    }

    public void SetResourceValue(string resourceName, float newValue)
    {
        if (resources.ContainsKey(resourceName))
        {
            resources[resourceName] = newValue;
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Resource: " + resourceName);
        }
    }

    public void ModifyResourceValue(string resourceName, float adjustment)
    {
        if (resources.ContainsKey(resourceName))
        {
            float newValue = resources[resourceName] + adjustment;

            resources[resourceName] = Mathf.Clamp(newValue, 0, GetMaxResourceValue(resourceName));
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Resource: " + resourceName);
        }
    }

    public float GetStatValue(string statName)
    {
        if (stats.ContainsKey(statName))
        {
            return stats[statName];
        }

        else
        {
            Debug.LogError("Entity " + name + " does not contain a definition for Stat: " + statName);
            return 0;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Bullet")
        {
            SetBool("isColliding", true);
        }

        if (collision.gameObject.tag == "Enemy")
        {
            ModifyResourceValue("hp", -10);
            collision.gameObject.GetComponent<Entity>().ModifyResourceValue("hp", -10);

            rb.velocity = (transform.position - collision.transform.position).normalized * 10;
        }
    }

    private void OnCollisionExit()
    {
        SetBool("isColliding", false);
    }

}
