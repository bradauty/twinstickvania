using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "TwinStickVania/Stats")]
public class EntityStats : SerializedScriptableObject
{
    //Default Stats
    public Dictionary<string, float> resources = new Dictionary<string, float>()
    {
        {"hp", 50f},
        {"mana", 100f},
        {"stamina", 100f},
        {"jumpCount", 1f }
    };

    public Dictionary<string, float> stats = new Dictionary<string, float>()
    {
        {"moveSpeed", 5f},
        {"sprintSpeed", 7.5f },
        {"jumpHeight", 5f },
        {"jumpTime", 0.5f },
        {"rotationTime", 0.05f},
        {"detectionRadius", 10f},
        {"scanRadius", 10f},
        {"fallMultiplier", 2f }
    };

    public Dictionary<string, float> cooldowns = new Dictionary<string, float>()
    {
        {"dashCooldown", 1f},
        {"jumpDelay", 0.3f}
    };
    
    public List<string> boolChecks = new List<string>()
    {
        "damageTaken",
        "isStaggered",
        "isGrounded",
        "isColliding",
        "canJump",
        "isJumping",
        "isFalling"
    };


    //String references to default stats for custom editors
    public static List<string> defaultResources = new List<string>()
    {
        "hp",
        "mana",
        "stamina",
        "jumpCount",
        "Custom Resource Name"
    };

    public static List<string> defaultStats = new List<string>()
    {
        "moveSpeed",
        "sprintSpeed",
        "jumpHeight",
        "jumpTime",
        "rotationTime",
        "detectionRadius",
        "scanRadius",
        "fallMultiplier",
        "Custom Stat Name"
    };

    public static List<string> defaultCooldowns = new List<string>()
    {
        "dashCooldown",
        "jumpDelay",
        "Custom Cooldown Name"
    };

    public static List<string> defaultBoolChecks = new List<string>()
    {
        "damageTaken",
        "isStaggered",
        "isGrounded",
        "isColliding",
        "canJump",
        "isJumping",
        "isFalling",
        "Custom Bool Name"
    };
}
