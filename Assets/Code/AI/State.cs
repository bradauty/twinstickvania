using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewState", menuName = "TwinStickVania/AI/State")]
public class State : ScriptableObject
{
    public Action[] actions;
    public Action[] actionInstances { get; private set; }

    public Transition[] transitions;

    public void EnterState(StateController controller)
    {
        actionInstances = new Action[actions.Length];
        
        for (int i = 0; i < actions.Length; i++)
        {
            actionInstances[i] = Instantiate(actions[i]);
            actionInstances[i].Begin(controller);
        }
    }

    public void UpdateState(StateController controller)
    {
        DoActions(controller);

        CheckTransitions(controller);
    }

    private void DoActions(StateController controller)
    {
        for (int i = 0; i < actionInstances.Length; i++)
        {
            actionInstances[i].Act(controller);
        }
    }

    private void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            int conditionsMet = 0;

            foreach (Decision decision in transitions[i].decisions)
            {
                if (decision.Decide(controller))
                {
                    conditionsMet += 1;
                }
            }

            if (conditionsMet == transitions[i].decisions.Length)
            {
                controller.TransitionToState(transitions[i].trueState);
            }

            else
            {
                controller.TransitionToState(transitions[i].falseState);
            }
        }
    }

    public void ExitState(StateController controller)
    {
        for (int i = 0; i < actionInstances.Length; i++)
        {
            actionInstances[i].End(controller);
            Destroy(actionInstances[i]);
        }
    }

}
