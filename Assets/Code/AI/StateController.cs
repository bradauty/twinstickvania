using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour
{
    public State initialAIState;
    public State initialPlayerState;
    public State initialSpawnState;

    public Animator animator { get; private set; }

    public State currentState { get; private set; }
    public State remainState;

    public float stateTimeElapsed { get; private set; }

    public Entity entity { get; private set; }

    bool active;

    void Awake()
    {
        entity = GetComponent<Entity>();

        animator = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        currentState = Instantiate(initialAIState);

        currentState.EnterState(this);

        active = true;
    }

    void Update()
    {
        if (!active)
            return;

        currentState.UpdateState(this);

        stateTimeElapsed += Time.deltaTime;
    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            if (currentState != null)
            {
                currentState.ExitState(this);
                Destroy(currentState);
            }

            currentState = Instantiate(nextState);
            currentState.EnterState(this);

            stateTimeElapsed = 0;
        }
    }

    public void ToggleActive()
    {
        active = !active;
    }
}
