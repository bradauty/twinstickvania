using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Dash")]
public class DashAction : Action
{
    public float dashSpeedMultiplier = 3;

    private Vector3 targetPos;

    public override void Begin(StateController controller)
    {
        controller.entity.mc.SetSpeedMultiplier(dashSpeedMultiplier);

        if (PlayerInputHandler.main.moveInput.magnitude > 0.1)
        {
            targetPos = controller.entity.transform.position + (PlayerInputHandler.main.moveInput.normalized * 50);
        }

        else
        {
            targetPos = controller.entity.transform.position + (-controller.entity.aimTransform.right * 50);
        }

        base.Begin(controller);
    }

    public override void Act(StateController controller)
    {
        controller.entity.mc.SetMoveTarget(targetPos);

        base.Act(controller);
    }

    public override void End(StateController controller)
    {
        controller.entity.mc.SetMoveTarget(controller.entity.transform.position);

        controller.entity.mc.SetSpeedMultiplier(1);

        controller.entity.ResetCooldown("dashCooldown");
        
        base.End(controller);
    }
}
