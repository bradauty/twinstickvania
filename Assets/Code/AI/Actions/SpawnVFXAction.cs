using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Should consider a separate solution for handling visual effects in future

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/SpawnVFX")]
public class SpawnVFXAction : Action
{
    public GameObject VFX;
    public bool parentToEntity = false;
    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        if (parentToEntity)
        {
            Instantiate(VFX, controller.transform.position, Quaternion.identity, controller.transform);
        }

        else
        {
            Instantiate(VFX, controller.transform.position, Quaternion.identity);
        }
    }
}
