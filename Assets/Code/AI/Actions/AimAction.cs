using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Aim")]
public class AimAction : Action
{
    public float timeScale = 1;

    public float aimTime = 0.5f;
    public float maxAimDistance = 5;

    private LineRenderer aimLine;
    private RaycastHit hit;
    private float currentDistance;

    //TEMP
    private GameObject highlight;

    public override void Begin(StateController controller)
    {
        aimLine = controller.entity.lineRenderer;
        aimLine.enabled = true;
        aimLine.positionCount = 2;
        aimLine.SetPositions(new[] { controller.entity.aimTransform.position, controller.entity.aimTransform.position });


        //Time.timeScale = timeScale;
        //Time.fixedDeltaTime *= timeScale;
        
        currentDistance = 0;

        //TEMP - Create a light
        highlight = new GameObject("Aim Highlight");
        highlight.AddComponent<Light>().intensity = 100;
        highlight.SetActive(false);
    }

    public override void Act(StateController controller)
    {
        Vector3 endPoint = controller.entity.aimTransform.position - controller.entity.aimTransform.right * currentDistance;

        if (Physics.Raycast(controller.entity.aimTransform.position, -controller.entity.aimTransform.right, out hit, currentDistance))
        {
            if (hit.collider.GetComponent<Entity>() != null)
            {
                Entity targetEntity = hit.collider.GetComponent<Entity>();
                controller.entity.SetFocusTransform(targetEntity.transform);

                //TEMP - stick light to enemy
                highlight.transform.position = targetEntity.transform.position;
                highlight.SetActive(true);
            }

            currentDistance = Vector3.Distance(controller.entity.aimTransform.position, hit.transform.position);

            endPoint = hit.point;
        
        }

        else
        {
            controller.entity.SetFocusTransform(null);

            highlight.SetActive(false);

            currentDistance = Mathf.Lerp(currentDistance, maxAimDistance, aimTime);

        }

        aimLine.SetPositions(new[] { controller.entity.aimTransform.position, endPoint });

    }

    public override void End(StateController controller)
    {
        aimLine.enabled = false;

        //TEMP - Destroy light
        Destroy(highlight);

        //Time.timeScale = 1;
        //Time.fixedDeltaTime /= timeScale;

        base.End(controller);
    }
}
