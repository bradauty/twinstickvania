using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/ModifyBool")]
public class ModifyBoolAction : Action
{
    private static List<string> boolStrings = EntityStats.defaultBoolChecks;
    private string customString = boolStrings[boolStrings.Count - 1];

    [ValueDropdown("boolStrings")]
    public string boolName;

    [HideIf("@this.boolName != this.customString")]
    public string customBoolName;

    enum Modification { toggle, setTrue, setFalse }
    [SerializeField] Modification modification;

    public bool reverseOnActionEnd;

    private string boolToChange;

    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        if (boolName != customString)
        {
            boolToChange = boolName;
        }

        else if (customBoolName != null && controller.entity.boolChecks.ContainsKey(customBoolName))
        {
            boolToChange = customBoolName;
        }

        else
        {
            Debug.LogError("Resource name for " + controller.name + "'s current ModifyBoolAction does not appear on the Entity");
        }

        switch (modification)
        {
            case Modification.toggle:
                controller.entity.SetBool(boolToChange, !controller.entity.GetBool(boolToChange));
                break;
            case Modification.setTrue:
                controller.entity.SetBool(boolToChange, true);
                break;
            case Modification.setFalse:
                controller.entity.SetBool(boolToChange, false);
                break;
            default:
                break;
        }
    }

    public override void End(StateController controller)
    {
        if (reverseOnActionEnd)
        {
            switch (modification)
            {
                case Modification.toggle:
                    controller.entity.SetBool(boolToChange, !controller.entity.GetBool(boolToChange));
                    break;
                case Modification.setTrue:
                    controller.entity.SetBool(boolToChange, false);
                    break;
                case Modification.setFalse:
                    controller.entity.SetBool(boolToChange, true);
                    break;
                default:
                    break;
            }
        }
        
        base.End(controller);
    }
}
