using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Patrol")]
public class PatrolAction : Action
{
    private PatrolZone patrolZone;
    private Transform currentWaypoint;

    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        patrolZone = controller.GetComponent<PatrolZone>();

        if (patrolZone != null)
        {
            currentWaypoint = patrolZone.waypointList[patrolZone.nextWaypoint];

            controller.entity.SetFocusTransform(currentWaypoint);
            controller.entity.mc.SetMoveTarget(currentWaypoint.position);
            controller.entity.mc.SetAimTarget(currentWaypoint.position);
        }

        else
        {
            Debug.LogError(controller.name + " is trying to use the Patrol action with no PatrolZone script attached");
        }
    }
    public override void Act(StateController controller)
    {
        float distanceToWaypoint;

        //if (controller.entity.movementType == Entity.MovementType.platformer)
        //{
        //    distanceToWaypoint = Mathf.Abs(controller.transform.position.x - patrolZone.waypointList[patrolZone.nextWaypoint].position.x);
        //}

        //else
        //{
        //    distanceToWaypoint = Vector3.Distance(controller.transform.position, patrolZone.waypointList[patrolZone.nextWaypoint].position);
        //}

        distanceToWaypoint = Vector3.Distance(controller.transform.position, patrolZone.waypointList[patrolZone.nextWaypoint].position);

        if (distanceToWaypoint <= 0.5f)
        {
            patrolZone.nextWaypoint = (patrolZone.nextWaypoint + 1) % patrolZone.waypointList.Count;

            currentWaypoint = patrolZone.waypointList[patrolZone.nextWaypoint];

            controller.entity.SetFocusTransform(currentWaypoint);
            controller.entity.mc.SetMoveTarget(currentWaypoint.position);
            controller.entity.mc.SetAimTarget(currentWaypoint.position);
        }
    }

    public override void End(StateController controller)
    {
        base.End(controller);

        controller.entity.ClearFocusTransform();
        controller.entity.mc.SetMoveTarget(controller.entity.transform.position);
    }
}
