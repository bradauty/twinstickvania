using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/ModifyResource")]
public class ModifyResourceAction : Action
{
    private static List<string> resourceStrings = EntityStats.defaultResources;
    private string customString = resourceStrings[resourceStrings.Count - 1];

    [ValueDropdown("resourceStrings")]
    public string resourceName;

    [HideIf("@this.resourceName != this.customString")]
    public string customResourceName;

    enum Modification { reset, set, modify, modifyPerSecond }
    [SerializeField] Modification modification;

    [HideIf("@this.modification == Modification.reset")]
    public float value = 10f;

    private string resourceToChange;

    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        if (resourceName != customString)
        {
            resourceToChange = resourceName;
        }

        else if (customResourceName != null && controller.entity.boolChecks.ContainsKey(customResourceName))
        {
            resourceToChange = customResourceName;
        }

        else
        {
            Debug.LogError("Resource name for " + controller.name + "'s current ModifyResourceAction does not appear on the Entity");
        }

        switch (modification)
        {
            case Modification.reset:
                controller.entity.ResetResourceValue(resourceToChange);
                break;
            case Modification.set:
                controller.entity.SetResourceValue(resourceToChange, value);
                break;
            case Modification.modify:
                controller.entity.ModifyResourceValue(resourceToChange, value);
                break;
            default:
                break;
        }
    }


    public override void Act(StateController controller)
    {
        if (modification == Modification.modifyPerSecond)
        {
            controller.entity.ModifyResourceValue(resourceToChange, value * Time.deltaTime);
        }

        base.Act(controller);
    }
}
