using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Fire")]
public class FireAction : Action
{
    [TextArea]
    public string devNote = "The Action Duration of this Action is overriden by the Weapon's 'rateOfFire' variable";
    public override void Begin(StateController controller)
    {
        actionLifetime = controller.entity.weapon.rateOfFire;
        
        base.Begin(controller);

        actionComplete = true;
    }

    public override void Act(StateController controller)
    {
        if (actionLifetime >= actionDuration)
        {
            controller.entity.weapon.Fire();

            actionComplete = false;
            actionDuration = 0;
        }
    }
}
