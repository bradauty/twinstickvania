using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Hunt")]
public class HuntAction : Action
{
    public override void Act(StateController controller)
    {
        if (controller.entity.currentFocus != null)
        {
            controller.entity.mc.SetMoveTarget(controller.entity.currentFocus.position);
            controller.entity.mc.SetAimTarget(controller.entity.currentFocus.position);
        }

        base.Act(controller);
    }

    public override void End(StateController controller)
    {
        controller.entity.mc.SetMoveTarget(controller.entity.transform.position + controller.entity.rb.velocity.normalized * 0.5f);
        controller.entity.mc.SetAimTarget(controller.entity.transform.position + controller.entity.rb.velocity.normalized * 2f);

        base.End(controller);
    }
}
