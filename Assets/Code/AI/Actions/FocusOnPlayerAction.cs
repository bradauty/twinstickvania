using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/FocusOnPlayer")]
public class FocusOnPlayerAction : Action
{
    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        controller.entity.SetFocusTransform(GameManager.main.player.transform);
    }
}
