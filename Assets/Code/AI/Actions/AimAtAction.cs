using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/AimAt")]
public class AimAtAction : Action
{
    public override void Act(StateController controller)
    {
        if (controller.entity.currentFocus != null)
        {
            controller.entity.mc.SetAimTarget(controller.entity.currentFocus.position);
        }

        base.Act(controller);
    }
}
