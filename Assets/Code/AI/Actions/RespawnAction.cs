using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Respawn")]
public class RespawnAction : Action
{
    Entity oldEntity;
    Entity newEntity;

    GameObject playerPrefab;

    Collider oldCollider;
    Collider newCollider;

    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        playerPrefab = Instantiate(GameManager.main.playerPrefab,controller.transform.position, Quaternion.identity);

        oldEntity = controller.entity;
        newEntity = playerPrefab.GetComponent<Entity>();

        oldCollider = oldEntity.GetComponent<Collider>();
        newCollider = newEntity.GetComponent<Collider>();

        newEntity.SetResourceValue("mana", 0);

        Physics.IgnoreCollision(oldCollider, newCollider, true);

        GameManager.main.ActivateEntity(newEntity);

        HUDManager.main.SetPlayerEntity(GameManager.main.player);

        StateController newPlayerSC = newEntity.GetComponent<StateController>();

        newPlayerSC.TransitionToState(Instantiate(newPlayerSC.initialSpawnState));
    }

    public override void Act(StateController controller)
    {
        oldEntity.SetResourceValue("mana", 0);
        oldEntity.SetResourceValue("hp", 0);

        base.Act(controller);
    }

    public override void End(StateController controller)
    {
        Physics.IgnoreCollision(oldCollider, newCollider, false);

        base.End(controller);
    }

}
