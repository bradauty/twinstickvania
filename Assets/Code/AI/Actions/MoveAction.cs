using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Move")]
public class MoveAction : Action
{
    public override void Act(StateController controller)
    {
        float sprintInput = PlayerInputHandler.main.dashInput;
        Vector3 moveInput = PlayerInputHandler.main.moveInput;

        if (sprintInput >= 0.2 && controller.entity.stats.ContainsKey("sprintSpeed"))
        {
            controller.entity.mc.SetSpeedMultiplier(controller.entity.stats["sprintSpeed"] / controller.entity.stats["moveSpeed"]);
        }

        else
        {
            controller.entity.mc.SetSpeedMultiplier(1);
        }

        if (moveInput.magnitude >= PlayerInputHandler.main.moveInputDeadZone)
        {
            Vector3 moveTarget = controller.entity.transform.position + moveInput;
            controller.entity.mc.SetMoveTarget(moveTarget);
        }

        base.Act(controller);
    }
}
