using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Jump")]
public class JumpAction : Action
{
    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        controller.entity.mc.Jump();
    }
}
