using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Spawn")]
public class SpawnAction : Action
{
    public GameObject[] spawnTable;

    public override void Act(StateController controller)
    {
        Spawn(controller);
    }

    private void Spawn(StateController controller)
    {
        for (int i = 0; i < spawnTable.Length; i++)
        {
            GameObject child = Instantiate(spawnTable[i], controller.transform.position + new Vector3(i, 0, 0), Quaternion.identity);

            child.transform.right = Vector3.right;
        }
    }
}
