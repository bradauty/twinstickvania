using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/Possess")]
public class PossessAction : Action
{
    Entity oldEntity;
    Entity newEntity;
    
    public override void Begin(StateController controller)
    {
        oldEntity = GameManager.main.player;
        newEntity = controller.entity.currentFocus.GetComponent<Entity>();

        oldEntity.SetResourceValue("mana", 0);
        oldEntity.SetResourceValue("hp", 0);

        newEntity.SetResourceValue("mana", newEntity.GetMaxResourceValue("mana"));
        newEntity.SetResourceValue("hp", newEntity.GetMaxResourceValue("hp"));

        newEntity.weapon.damageEnemies = true;
        newEntity.weapon.damagePlayer = false;
        newEntity.weapon.UpdateProjectileStats();

        //Changes Layer and Tag of possessed entity to Player
        newEntity.gameObject.layer = 3;
        newEntity.gameObject.tag = "Player";

        GameManager.main.ActivateEntity(newEntity);

        HUDManager.main.SetPlayerEntity(GameManager.main.player);

        base.Begin(controller);
    }

}
