using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Action/EmitForce")]
public class EmitForceAction : Action
{
    public float explosionForce = 50;
    public float explosionRadius = 5;
    public override void Begin(StateController controller)
    {
        base.Begin(controller);

        Collider[] colliders = Physics.OverlapSphere(controller.transform.position, explosionRadius);

        foreach (var collider in colliders)
        {
            Rigidbody rb = collider.attachedRigidbody;
            
            if(rb != null && rb != controller.entity.rb)
            {
                collider.attachedRigidbody.AddExplosionForce(explosionForce, controller.transform.position, explosionRadius, 0, ForceMode.Impulse);
            }
        }
    }
}
