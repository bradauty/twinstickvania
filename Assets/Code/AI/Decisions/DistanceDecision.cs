using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/DistanceState")]
public class DistanceDecision : Decision
{
    public float minDistance = 5f;
    public float maxDistance = 10f;
    public override bool Decide(StateController controller)
    {
        float distance = 0f;

        if(controller.entity.currentFocus != null)
        {
            distance = Vector3.Distance(controller.transform.position, controller.entity.currentFocus.transform.position);
        }

        return distance >= minDistance && distance <= maxDistance;
    }
}
