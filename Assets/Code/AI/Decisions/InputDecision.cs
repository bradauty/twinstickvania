using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/Input")]
public class InputDecision : Decision
{
    public enum InputTypes {Move, Attack, Dash, Ranged, Spell, Jump};
    public InputTypes inputType = InputTypes.Move;

    public override bool Decide(StateController controller)
    {
        return DetectInput(controller);
    }

    private bool DetectInput(StateController controller)
    {
        if (inputType == InputTypes.Move
            && PlayerInputHandler.main.moveInput.magnitude >= PlayerInputHandler.main.moveInputDeadZone)
        {
            return true;
        }

        if (inputType == InputTypes.Attack
            && PlayerInputHandler.main.attackInput >= 0.1f)
        {
            return true;
        }

        if (inputType == InputTypes.Dash
            && PlayerInputHandler.main.dashInput >= 0.1f)
        {
            return true;
        }

        if (inputType == InputTypes.Ranged
            && PlayerInputHandler.main.rangedInput >= 0.1f)
        {
            return true;
        }
        if (inputType == InputTypes.Spell
            && PlayerInputHandler.main.spellInput >= 0.1f)
        {
            return true;
        }

        if (inputType == InputTypes.Jump
            && PlayerInputHandler.main.jumpInput >= 0.1f)
        {
            return true;
        }

        else
        {
            return false;
        }
    }
}
