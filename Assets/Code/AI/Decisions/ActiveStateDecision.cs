using ES3Types;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/ActiveState")]
public class ActiveStateDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool chaseTargetIsActive = GameManager.main.player.gameObject.activeSelf;

        return chaseTargetIsActive;
    }
}
