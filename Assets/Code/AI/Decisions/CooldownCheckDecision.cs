using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/CooldownCheck")]
public class CooldownCheckDecision : Decision
{
    private static List<string> cooldownStrings = EntityStats.defaultCooldowns;
    private string customString = cooldownStrings[cooldownStrings.Count - 1];

    [ValueDropdown("cooldownStrings")]
    public string cooldownName;

    [DisableIf("@this.cooldownName != this.customString")]
    public string customCooldownName;

    public override bool Decide(StateController controller)
    {
        if (cooldownName != customString)
        {
            return controller.entity.CheckCooldown(cooldownName);
        }

        else if (customCooldownName != null && controller.entity.boolChecks.ContainsKey(customCooldownName))
        {
            return controller.entity.CheckCooldown(customCooldownName);
        }

        else
        {
            Debug.LogError("Cooldown name for " + controller.name + "'s current CooldownCheckDecision does not appear on the Entity");
            return false;
        }

    }
}
