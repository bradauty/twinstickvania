using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/StateTimeElapsed")]
public class StateTimeElapsedDecision : Decision
{
    public float timeElapsed;
    public override bool Decide(StateController controller)
    {
        return controller.stateTimeElapsed >= timeElapsed;
    }
}
