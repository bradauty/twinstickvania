using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/Look")]
public class LookDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Look(controller);
        return targetVisible;
    }

    private bool Look(StateController controller)
    {
        RaycastHit hit;

        Debug.DrawRay(controller.entity.transform.position, controller.transform.right.normalized * controller.entity.GetResourceValue("detectionRange"), Color.green);

        if (Physics.SphereCast(controller.entity.transform.position, 0.5f, controller.transform.right, out hit, controller.entity.GetResourceValue("detectionRange"))
            && hit.collider.CompareTag("Player"))
        {
            controller.entity.mc.SetMoveTarget(hit.transform.position);
            controller.entity.mc.SetRotationTarget(hit.transform.position);

            controller.entity.SetFocusTransform(hit.transform);

            return true;
        }
        else
        {
            controller.entity.ClearFocusTransform();

            return false;
        }
    }
}
