using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/EntityFocus")]
public class EntityFocusDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return controller.entity.currentFocus != null;
    }
}
