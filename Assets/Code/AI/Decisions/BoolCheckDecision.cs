using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/BoolCheck")]
public class BoolCheckDecision : Decision
{
    public enum ToCheck { self, target}
    public ToCheck toCheck = ToCheck.self;
    
    private static List<string> boolCheckStrings = EntityStats.defaultBoolChecks;
    private string customString = boolCheckStrings[boolCheckStrings.Count - 1];

    [ValueDropdown("boolCheckStrings")]
    public string boolName;

    [HideIf("@this.boolName != this.customString")]
    public string customBoolName;

    public override bool Decide(StateController controller)
    {
        Entity checkEntity = null;

        if (toCheck == ToCheck.self)
        {
            checkEntity = controller.entity;
        }

        else if (toCheck == ToCheck.target && controller.entity.currentFocus != null)
        {
            Entity targetEntity = controller.entity.currentFocus.GetComponent<Entity>();

            if (targetEntity != null)
            {
                checkEntity = targetEntity;
            }
        }

        if(checkEntity == null)
        {
            return false;
        }
        
        if(boolName != customString)
        {
            return checkEntity.GetBool(boolName);
        }

        else if (customBoolName != null && controller.entity.boolChecks.ContainsKey(customBoolName))
        {
            return checkEntity.GetBool(customBoolName);
        }

        else
        {
            Debug.LogError("Bool name for " + controller.name + "'s current BoolCheckDecision does not appear on the Entity");
            return false;
        }

    }
}
