using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/ActionComplete")]
public class ActionCompleteDecision : Decision
{
    public Action action;

    private Action originalAction;
    private bool actionFound;

    private void Awake()
    {
        originalAction = action;
    }

    public override bool Decide(StateController controller)
    {
        return CheckActionStatus(controller); ;
    }

    private bool CheckActionStatus(StateController controller)
    {
        if (action != null
            && action == originalAction)
        {
            foreach (Action actionInstance in controller.currentState.actionInstances)
            {
                if (actionInstance.name == action.name)
                {
                    action = actionInstance;
                    actionFound = true;
                }
            }
        }

        if (actionFound)
        {
            return action.actionComplete;
        }
        
        if (action == null)
        {
            foreach (Action actionInstance in controller.currentState.actionInstances)
            {
                if (actionInstance.actionDuration != 0)
                {
                    return actionInstance.actionComplete;
                }
            }
        }

        return false;
    }

}
