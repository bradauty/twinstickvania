using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/ResourceCheck")]
public class ResourceCheckDecision : Decision
{
    private static List<string> resourceStrings = EntityStats.defaultResources;
    private string customString = resourceStrings[resourceStrings.Count - 1];

    [ValueDropdown("resourceStrings")]
    public string resourceName;

    [HideIf("@this.resourceName != this.customString")]
    public string customResourceName;

    enum Operation { lowerThan, greatherThan, equals}
    [SerializeField] Operation check;
    [SerializeField] bool maxValue;
    [HideIf("@maxValue")]
    [SerializeField] float value;

    public override bool Decide(StateController controller)
    {
        float valueToCheck = 0;

        if (resourceName != customString)
        {
            valueToCheck = controller.entity.GetResourceValue(resourceName);

            if (maxValue)
            {
                value = controller.entity.GetMaxResourceValue(resourceName);
            }
        }

        else if (customResourceName != null && controller.entity.boolChecks.ContainsKey(customResourceName))
        {
            valueToCheck = controller.entity.GetResourceValue(customResourceName);

            if (maxValue)
            {
                value = controller.entity.GetMaxResourceValue(customResourceName);
            }
        }

        else
        {
            Debug.LogError("Resource name for " + controller.name + "'s current ResourceCheckDecision does not appear on the Entity");
        }

        switch (check)
        {
            case Operation.lowerThan:
                return valueToCheck <= value;
            case Operation.greatherThan:
                return valueToCheck >= value;
            case Operation.equals:
                return valueToCheck == value;
            default:
                return false;
        }
    }
}
