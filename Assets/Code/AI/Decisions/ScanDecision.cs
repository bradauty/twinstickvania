using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;

[CreateAssetMenu(menuName = "TwinStickVania/AI/Decision/Scan")]
public class ScanDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        bool targetVisible = Scan(controller);
        return targetVisible;
    }

    private bool Scan(StateController controller)
    {
        bool playerFound = false;
        //Transform target = null;
        
        Collider[] colliders = Physics.OverlapSphere(controller.transform.position, controller.entity.stats["scanRadius"]);

        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].CompareTag("Player"))
            {
                playerFound = true;
                //target = colliders[i].transform;
            }
        }

        return (playerFound);

        //if (playerFound)
        //{
        //    //controller.entity.SetFocusTransform(target);

        //    //controller.entity.SetMoveTarget(target.position);
        //    //controller.entity.SetRotationTarget(target.position);

        //    return true;
        //}
        //else
        //{
        //    //controller.entity.ClearFocusTransform();

        //    return false;
        //}
    }

}
