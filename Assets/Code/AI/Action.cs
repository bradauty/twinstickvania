using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action : ScriptableObject
{
    [Header("Action Settings")]
    public string animatorBoolName;
    public float actionDuration;
    public float actionCost;

    [HideInInspector] public bool actionComplete;
    [HideInInspector] public float actionLifetime;
    private bool animationBoolFound = false;

    //Called as we transition into the current State
    public virtual void Begin(StateController controller)
    {
        if (animatorBoolName != null && controller.animator != null)
        {
            for (int i = 0; i < controller.animator.parameters.Length; i++)
            {
                if (controller.animator.GetParameter(i).name == animatorBoolName)
                {
                    animationBoolFound = true;

                    controller.animator.SetBool(animatorBoolName, true);

                    i += controller.animator.parameters.Length;
                }
            }
        }

        actionLifetime = 0;
        actionComplete = false;
    }

    //Called while in active State
    public virtual void Act(StateController controller)
    {
        if (actionDuration != 0)
        {
            actionLifetime += Time.deltaTime;

            if (actionLifetime >= actionDuration)
            {
                actionComplete = true;
            }
        }
    }

    //Called after transition conditions are met as we leave the current State
    public virtual void End(StateController controller)
    {
        if (animationBoolFound)
        {
            controller.animator.SetBool(animatorBoolName, false);
        }
    }
}
