using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovementController : MonoBehaviour
{
    [HideInInspector] public Entity entity;
    public Vector3 moveTarget { get; private set; }
    public Vector3 rotationTarget { get; private set; }
    public Rigidbody rb { get; private set; }

    [HideInInspector] public float moveSpeedMultiplier;
    [HideInInspector] public Collider entityCollider;

    private void Start()
    {
        Setup();
    }
    private void FixedUpdate()
    {
        UpdateMethods();
    }

    public virtual void Setup()
    {
        rb = GetComponent<Rigidbody>();

        entityCollider = GetComponent<Collider>();

        entity = GetComponent<Entity>();

        moveTarget = rotationTarget = transform.position;
    }

    public virtual void UpdateMethods()
    {
        MoveTowardsTarget();
        FaceTowardsTarget();
        RotateTowardsTarget();
    }

    public virtual void MoveTowardsTarget()
    {
        float distToTarget = Vector3.Distance(moveTarget, transform.position);
        int dir = 0;

        if (moveTarget.x - transform.position.x > 0.1)
        {
            dir = 1;
        }

        else if (moveTarget.x - transform.position.x < 0.1)
        {
            dir = -1;
        }

        else
        {
            SetMoveTarget(transform.position);
            rb.velocity = Vector3.zero;
        }

        if (Vector3.Distance(moveTarget, transform.position) >= 0.1)
        {
            rb.velocity = new Vector3(dir * entity.GetStatValue("moveSpeed") * moveSpeedMultiplier * Mathf.Clamp01(distToTarget), rb.velocity.y, 0);
        }
    }

    public virtual void RotateTowardsTarget()
    {
        if (entity.weapon != null)
        {
            entity.weapon.transform.rotation = Quaternion.Euler(rotationTarget);
        }

        if (entity.aimTransform != null)
        {
            entity.aimTransform.rotation = Quaternion.Euler(rotationTarget);
        }
    }

    public virtual void FaceTowardsTarget()
    {
        float xDir = (Quaternion.Euler(rotationTarget) * -Vector3.right).x;

        if (xDir > 0)
        {
            entity.transform.rotation = Quaternion.LookRotation(-Vector3.forward, Vector3.up);
        }

        else if (xDir < 0)
        {
            entity.transform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
        }
    }

    public virtual void SetMoveTarget(Vector3 newMoveTarget)
    {
        moveTarget = newMoveTarget;
    }

    public virtual void SetRotationTarget(Vector3 newRotationTarget)
    {
        rotationTarget = newRotationTarget;
    }

    public virtual void SetAimTarget(Vector3 newAimTarget)
    {
        Vector2 aimVector = new Vector2(transform.position.x - newAimTarget.x, transform.position.y - newAimTarget.y);
        float aimOffset = Mathf.Atan2(aimVector.y, aimVector.x) * Mathf.Rad2Deg;
        rotationTarget = new Vector3(0, 0, aimOffset);
    }

    public virtual void SetSpeedMultiplier(float multi)
    {
        moveSpeedMultiplier = multi;
    }

    public virtual void Jump()
    {
        //Does nothing at default
    }

    public virtual void BoostVelocity(Vector3 velocityBoost)
    {
        if (rb.velocity.x <= velocityBoost.x)
        {
            rb.velocity = new Vector3(velocityBoost.x, rb.velocity.y);
        }

        else
        {
            rb.velocity += velocityBoost;
        }

        if (rb.velocity.y <= velocityBoost.y)
        {
            rb.velocity = new Vector3(rb.velocity.x, velocityBoost.y);
        }

        else
        {
            rb.velocity += velocityBoost;
        }
    }
}
