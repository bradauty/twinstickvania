using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillTimer : MonoBehaviour
{
    public float timeToKill = 5f;

    void Update()
    {
        timeToKill -= Time.deltaTime;

        if (timeToKill <= 0)
        {
            Destroy(this);
        }
    }
}
