using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolZone : MonoBehaviour
{
    public List<Transform> waypointList;
    [HideInInspector] public int nextWaypoint;
}
