using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MC_Platformer : MovementController
{
    
    float gravityY;
    float fallMulti;

    
    public override void Setup()
    {
        base.Setup();

        gravityY = -2 * entity.GetStatValue("jumpHeight") / Mathf.Pow(entity.GetStatValue("jumpTime"), 2);
        fallMulti = entity.GetStatValue("fallMultiplier");
    }

    public override void UpdateMethods()
    {
        base.UpdateMethods();

        //Apply normal gravity
        if (rb.velocity.y >= 0)
        {
            rb.velocity += Vector3.up * gravityY * Time.deltaTime;
            entity.SetBool("isFalling", false);
        }

        //Apply speedy fall
        else
        {
            entity.SetBool("isFalling", true);
            entity.SetBool("isJumping", false);

            rb.velocity += Vector3.up * gravityY * (fallMulti - 1) * Time.deltaTime;
        }

        CheckIfGrounded();
    }

    private void CheckIfGrounded()
    {
        bool boxCastSuccess = Physics.BoxCast(entityCollider.bounds.center, entityCollider.bounds.extents, Vector3.down, transform.rotation, 0.5f, 1 << 6);

        if (boxCastSuccess)
        {
            entity.SetBool("isGrounded", true);
            entity.SetBool("isFalling", false);
            entity.SetBool("isJumping", false);
            entity.ResetResourceValue("jumpCount");
        }
    }

    public override void Jump()
    {
        if ((entity.GetBool("isGrounded") || entity.GetResourceValue("jumpCount") > 0) && entity.CheckCooldown("jumpDelay"))
        {
            rb.velocity = (Vector3.up * 2 * entity.GetStatValue("jumpHeight")) / entity.GetStatValue("jumpTime");
            entity.ModifyResourceValue("jumpCount", -1);
            entity.SetBool("isGrounded", false);
            entity.SetBool("isJumping", true);
            entity.ResetCooldown("jumpDelay");
        }

        base.Jump();
    }
}
