using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public static HUDManager main;
    
    public Slider healthBar;
    public Slider manaBar;

    public RectTransform crosshair;

    private Entity player;
    private float maxHp;
    private float maxMana;

    void Start()
    {
        main = this;

        SetPlayerEntity(GameManager.main.player);

        RefreshMaxResourceValues();
    }

    void Update()
    {
        UpdateHUDValues();

        crosshair.position = PlayerInputHandler.main.pointInput;
        crosshair.rotation = GameManager.main.player.weapon.transform.rotation;
    }

    public void SetPlayerEntity(Entity newPlayer)
    {
        player = newPlayer;
        RefreshMaxResourceValues();
    }

    public void RefreshMaxResourceValues()
    {
        maxHp = player.GetMaxResourceValue("hp") ;
        maxMana = player.GetMaxResourceValue("mana");
    }

    public void UpdateHUDValues()
    {
        healthBar.value = player.GetResourceValue("hp") / maxHp;
        manaBar.value = player.GetResourceValue("mana") / maxMana;
    }
}
