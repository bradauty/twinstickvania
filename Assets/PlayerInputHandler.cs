using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using DG.Tweening;

public class PlayerInputHandler : MonoBehaviour
{
    public static PlayerInputHandler main;

    PlayerInput input;

    [Range (0,1)] public float moveInputDeadZone;

    Entity player;

    public Vector3 moveInput { get; private set; }
    public Vector3 aimInput { get; private set; }
    public Vector2 pointInput { get; private set; }
    public float attackInput { get; private set; }
    public float rangedInput { get; private set; }

    public float jumpInput { get; private set; }
    public float dashInput { get; private set; }
    public float spellInput { get; private set; }

    bool mouseInput = true;

    void Awake()
    {
        input = new PlayerInput();

        main = this;
    }

    private void Start()
    {
        ControlNewEntity(GameManager.main.player);
    }

    private void OnEnable()
    {
        input.Player.Enable();

        input.Player.AimGamepad.performed += AimGamepad;
    }

    private void OnDisable()
    {
        input.Player.Disable();
    }

    void Update()
    {
        //Check this isn't called every frame while Gamepad is active
        //Needs to be called every frame because it is driven by player and camera positions, which are not always in sync
        if (mouseInput)
        {
            AimMouse();
        }
        
        attackInput = input.Player.Attack.ReadValue<float>();
        moveInput = input.Player.Move.ReadValue<Vector2>();
        aimInput = input.Player.AimGamepad.ReadValue<Vector2>();
        pointInput = input.Player.AimMouse.ReadValue<Vector2>();
        rangedInput = input.Player.Ranged.ReadValue<float>();
        jumpInput = input.Player.Jump.ReadValue<float>();
        dashInput = input.Player.Dash.ReadValue<float>();
        spellInput = input.Player.Spell.ReadValue<float>();
    }

    public void ControlNewEntity(Entity entity)
    {
        player = entity;
    }

    private void AimGamepad(InputAction.CallbackContext obj)
    {
        mouseInput = false;
        
        float aimOffset = Mathf.Atan2(-aimInput.y, -aimInput.x) * Mathf.Rad2Deg;
        Vector3 targetRot = new Vector3(0, 0, aimOffset);

        player.mc.SetRotationTarget(targetRot);
    }

    private void AimMouse()
    {
        Vector3 rotateTransformPosition = Camera.main.WorldToScreenPoint(player.aimTransform.position);
        Vector2 aimVector = new Vector2(rotateTransformPosition.x - pointInput.x, rotateTransformPosition.y - pointInput.y);
        float aimOffset = Mathf.Atan2(aimVector.y, aimVector.x) * Mathf.Rad2Deg;
        Vector3 targetRot = new Vector3(0, 0, aimOffset);

        player.mc.SetRotationTarget(targetRot);
    }
}
