using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    public float damage = 10f;
    public float force = 5f;

    private void Update()
    {
        transform.position = GameManager.main.player.transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        string colliderTag = collision.gameObject.tag;
        Rigidbody colliderRb = collision.rigidbody;

        //Apply force
        if (colliderRb != null && colliderTag != "Bullet")
        {
            Vector3 finalForce = (collision.transform.position - transform.position).normalized * force;

            colliderRb.AddForceAtPosition(finalForce, transform.position);

            colliderRb.AddExplosionForce(force, transform.position, 1);
        }

        if (colliderTag == "Enemy")
        {
            collision.gameObject.GetComponent<Entity>().ModifyResourceValue("hp", damage);
        }

    }
}
