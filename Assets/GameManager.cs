using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Entity player;

    public static GameManager main;
    public GameObject playerPrefab;

    private void Awake()
    {
        main = this;
    }

    private void Start()
    {
        ActivateEntity(player);

        Cursor.visible = false;
    }

    public void ActivateEntity (Entity newActiveEntity)
    {
        player = newActiveEntity;

        var newPlayerSC = player.GetComponent<StateController>();

        PlayerCam.main.SetNewFocusTransform(player.transform);
        PlayerInputHandler.main.ControlNewEntity(player);
        HUDManager.main.SetPlayerEntity(player);

        newPlayerSC.TransitionToState(Instantiate(newPlayerSC.initialPlayerState));
    }
}
